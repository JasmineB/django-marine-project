# Generated by Django 4.1.7 on 2023-03-16 22:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0005_health_animal_reporter_delete_animallist_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='health',
            old_name='reproted_on',
            new_name='reported_on',
        ),
    ]
