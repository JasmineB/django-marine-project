from django.db import models
# from django.conf import settings
from django.contrib.auth.models import User




# This class is for people to make a list of reports
class Report(models.Model):
    critical = models.BooleanField(default=False)
    alone = models.BooleanField(default=False)
    malnurished = models.BooleanField(default=False)
    wellnesscheck = models.BooleanField(default=False)
    location = models.CharField(max_length=200, blank=True)
    # created_on = models.DateTimeField(auto_now_add=True)
    notes = models.TextField()
    picture = models.URLField(blank=True)
    # animal = models.ForeignKey(
    #     Animal,
    #     related_name="condition",
    #     on_delete=models.CASCADE,
    #     null=True,
    # )
    reporter = models.ForeignKey(
        User,
        related_name="sighting",
        on_delete=models.CASCADE,
        null=True,
    )



# This class is for staff to make exams for each report
class Exam(models.Model):
    # exam_date = models.DateTimeField(auto_now_add=True)
    specie = models.CharField(max_length=200, blank=True)
    size = models.CharField(max_length=200, blank=True)
    weight = models.CharField(max_length=200, blank=True)
    age = models.CharField(max_length=200, blank=True)
    sex = models.CharField(max_length=200, blank=True)
    bloodworkrequired = models.BooleanField(default=False)
    pen = models.CharField(max_length=200, blank=True)
    notes = models.TextField()
    medicalpicture = models.URLField(blank=True)
    medicalreporter = models.ForeignKey(
        User,
        related_name="medical",
        on_delete=models.CASCADE,
        null=True,
    )
    animal = models.ForeignKey(
        Report,
        related_name="exams",
        on_delete=models.CASCADE,
        null=True,
    )
    # owner = models.ForeignKey(
    #     User, related_name="projects", on_delete=models.CASCADE, null=True
    # )
    # company = models.ForeignKey(
    #     Company,
    #     related_name = "projects",
    #     on_delete=models.CASCADE,
    #     null=True
    # )

    def __str__(self):
        return self.animal
