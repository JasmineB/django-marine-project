from django.shortcuts import render, redirect
from animals.models import Exam, Report
from animals.forms import ExamForm

def show_animal(request, id):
    single_animal = Exam.objects.get(id=id)
    context = {
        "single_animal":single_animal,
    }
    return render(request, "animals/detail.html", context)


def health_list(request):
    health_condition = Report.objects.all()
    context = {
        "health_condition": health_condition,
    }
    return render(request, "animals/health_list.html", context)

def mainpage(request):
    return render(request, "animals/main.html")


def create_animal_exam(request):
    if request.method == "POST":
        form = ExamForm(request.POST)
        if form.is_valid():
            exam = form.save()
            return redirect("show_animal", id=exam.animal.id)
    else:
        form = ExamForm()

    context = {
        "form": form
    }

    return render(request, "animals/create_exam.html", context)
