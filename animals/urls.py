from django.urls import path
from animals.views import show_animal, health_list, mainpage, create_animal_exam

urlpatterns = [
    # path("", show_animal),
    path("<int:id>/", show_animal, name="show_animal"),
    path("health/", health_list, name="health_list"),
    path("home/", mainpage, name="mainpage"),
    path("exams/create/", create_animal_exam, name="create_animal_exam")


]
