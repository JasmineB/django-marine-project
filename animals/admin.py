from django.contrib import admin
from animals.models import Exam, Report

@admin.register(Exam)
class ExamAdmin(admin.ModelAdmin):
    list_display = (

        "specie",
        "size",
        "weight",
        "age",
        "sex",
        "bloodworkrequired",
        "pen",
        "notes",
        "medicalpicture",
        "medicalreporter",
        "animal",
        "id",
    )






@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = (
        "critical",
        "alone",
        "malnurished",
        "wellnesscheck",
        "location",
        # "reported_on",
        "notes",
        "picture",
        # "animal",
        "reporter",
        "id",
    )
