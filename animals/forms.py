from django.forms import ModelForm
from animals.models import Exam


class ExamForm(ModelForm):
    class Meta:
        model = Exam
        fields = [

            "size",
            "weight",
            "age",
            "sex",
            "bloodworkrequired",
            "pen",
            "notes",
            "medicalpicture",
            "medicalreporter",
            "animal",

        ]
